# Adapters AdapterViews
## [Simplest ListView 1](SimpleListView1)
A very simple example of using a ListView with text only that
uses an ArrayAdapter and a listener  that pops up a Toast
## [Simple ListView 2](SimpleListView2)
Another simple ListView with text only.  In this one the ArrayList is
manipulated dynamically by the listener and some buttons on the UI
## [Simple GridView](SimpleGridView)
Uses a GridView with an adapter for images using a custom adapter.
## [Simple Custom Adapter + ListView](SimpleCustomLV)
Another simple ListView with image and text using a custom adapter.
There is a listener set on the rows in the adapter.
## [Simple Custom Adapter + ListView + Context Menu](SimpleCustomLVContextMenu)
Same as the previous code: ListView with image & text using a custom adapter.
Instead of a listener a Context menu is attached to the ListView.  
## [Simple Custom Adapter + RecyclerView](SampleRecyclerView)
A sample of using a RecyclerView with a custom adapter.
TODO:  as an exercise add an image to the RecyclerView layout
